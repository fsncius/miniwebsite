FROM python:3-onbuild
COPY api.py /usr/src/app
COPY requirements.txt /usr/src/app
CMD ["python","api.py"]

